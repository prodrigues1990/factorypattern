﻿using Autofac;
using ConsoleApplication.Loggers;
using SimulationModule;
using SimulationModule.Logging;
using System;

namespace ConsoleApplication
{
    class Program
    {
        private static IContainer container;
        private static Simulation simulation;

        static void Main(string[] args)
        {
            Bootstrap();

            int result = simulation.Run();
            Console.WriteLine("Simulation result: {0}", result);
            Console.ReadKey();
        }

        private static void Bootstrap()
        {
            BuildDependencyContainer();
            LogManager.Container = container;

            simulation = new Simulation();
        }

        private static void BuildDependencyContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new SimModule());
            builder.RegisterType<ConsoleLogger>().As<ILog>();

            container = builder.Build();
        }
    }
}
