﻿using SimulationModule.Logging;
using System;

namespace ConsoleApplication.Loggers
{
    internal class ConsoleLogger : ILog
    {
        public void Log(LogLevel level, string message)
        {
            Console.WriteLine("{0}:{1}", level.ToString().ToUpper(), message);
        }
    }
}
