﻿using SimulationModule.Logging;
using System;
using System.Threading;

namespace SimulationModule
{
    public class Simulation
    {
        private readonly ILog logger = LogManager.getLogger();

        private readonly Random random = new Random();

        public int Run()
        {
            logger.Log(LogLevel.Info, "generating simulation seed..");
            int seed = random.Next();

            // sleep for a random number of seconds, between 0 - 5
            // (simulate a simulation)
            int sleep = seed % 5 * 1000;
            logger.Log(LogLevel.Info, "starting simulation..");
            logger.Log(LogLevel.Debug, string.Format("seed:{0}", seed));
            Thread.Sleep(sleep);

            // (simulate a simulation result)
            int result = new Random(seed).Next(seed);
            logger.Log(
                LogLevel.Info,
                string.Format("simulation finished in {0}ms", sleep));
            return result;
        }
    }
}
