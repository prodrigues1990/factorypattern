﻿using Autofac;
using SimulationModule.Logging;
using SimulationModule.Logging.Loggers;

namespace SimulationModule
{
    public class SimModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<NoLogger>()
                .As<ILog>()
                .InstancePerRequest();
        }
    }
}
