﻿namespace SimulationModule.Logging.Loggers
{
    internal class NoLogger : ILog
    {
        public void Log(LogLevel level, string message)
        { }
    }
}
