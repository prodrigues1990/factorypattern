﻿namespace SimulationModule.Logging
{
    public interface ILog
    {
        void Log(LogLevel level, string message);
    }
}
