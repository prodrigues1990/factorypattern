﻿namespace SimulationModule.Logging
{
    public abstract class LoggerFactory
    {
        public static LoggerFactory Factory { private get; set; }

        public static ILog getLogger()
        {
            if (Factory == null)
                return new NoLogger();
            return Factory.getLogger();
        }
    }
}
