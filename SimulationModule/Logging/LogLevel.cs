﻿namespace SimulationModule.Logging
{
    public enum LogLevel
    {
        Debug,
        Trace,
        Info,
        Warning,
        Error,
        Fatal
    }
}
