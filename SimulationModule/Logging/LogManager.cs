﻿using Autofac;

namespace SimulationModule.Logging
{
    public static class LogManager
    {
        public static IContainer Container { private get; set; }

        public static ILog getLogger()
        {
            return Container.Resolve<ILog>();
        }
    }
}
